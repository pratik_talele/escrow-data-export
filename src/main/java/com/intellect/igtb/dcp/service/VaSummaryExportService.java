package com.intellect.igtb.dcp.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.mockito.internal.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intellect.igtb.dcp.cloudbocommons.exception.CBXBusinessException;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXException;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult.val_status;
import com.intellect.igtb.dcp.export.model.ExportContext;
import com.intellect.igtb.dcp.export.model.Title;
import com.intellect.igtb.dcp.export.service.CBXSExportService;
import com.intellect.igtb.dcp.export.service.ReportService;

@Component
public class VaSummaryExportService extends CBXSExportService {

	/** logger */
	private static final Logger logger = LoggerFactory.getLogger(VaSummaryExportService.class);

	/** instance of Utils class. */
	private com.intellect.igtb.dcp.utils.Utils utils = new com.intellect.igtb.dcp.utils.Utils();

	HashMap<String, FieldValidationResult> valResponseMap = new HashMap<String, FieldValidationResult>();

	@Autowired
	ReportService reportService;

	@Override
	public ExportContext init(final Map<String, String> httpHeaders, final Map<String, Object> payload)
			throws CBXException {
		logger.debug("IN init - VaSummaryService {}", payload.toString());
		exportContext = super.init(httpHeaders, payload);

		logger.debug("OUT init - VaSummaryService");
		return exportContext;
	}

	@Override
	public Map<String, FieldValidationResult> validate() {
		logger.debug("IN -> CreateVirtAcctCommand.validate");
		final FieldValidationResult valResult = new FieldValidationResult(val_status.OK);
		valResponseMap = new HashMap<String, FieldValidationResult>();
		Map<String, Object> payload = exportContext.getReq_payload();
		String format = (String) payload.get("format");

		if (format == null || format.isEmpty()) {
			valResult.setFieldName("format");
			valResult.setErrorCode("format required");
			valResult.setStatus(val_status.FAIL);
			String fieldMessage = "format Required";
			valResult.setErrorMessage(fieldMessage);
		}
		// add validation result into Map
		utils.addValidationResult(valResult, valResponseMap);
		logger.debug("OUT -> CreateVirtAcctCommand.validate");
		return valResponseMap;
	}

	@Override
	public String getQueryName(Map<String, Object> payload) {

		String groupCriteria = (String) payload.get("groupCriteria");
		String querName = "gb_physacc";
		if (StringUtils.isNotBlank(groupCriteria) && StringUtils.isNotEmpty(groupCriteria)) {
			if (groupCriteria.equals("curency")) {
				querName = "gb_currency";
			} else if (groupCriteria.equals("country")) {
				querName = "gb_country";

			} else if (groupCriteria.equals("entity")) {
				querName = "gb_entity";
			}
		}
		return querName;

	}

	@Override
	public String getFormat(Map<String, Object> payload) {
		String format = (String) payload.get("format");
		return format;
	}

	@Override
	public Boolean isGrouped(Map<String, Object> payload) {
		Boolean isGrouped = (Boolean) payload.get("isGrouped");
		return isGrouped;
	}

	@Override
	public Title getTitle(Map<String, Object> payload) {
		Title title = new Title();
		title.setKey((String) payload.get("summary_name"));
		return title;
	}

	@Override
	public String getHeaderBlocksConfig(Map<String, Object> payload) {

		return null;
	}

	@Override
	public String getGridConfigAsString(Map<String, Object> payload) {
		String sGridConfigData = reportService.loadContent("gridConfig.json");
		return sGridConfigData;
	}

	@Override
	public String getHeaderData(Map<String, Object> payload) {
		// String sHeaderData = reportService.loadContent("headerBlockData.json");
		return null;
	}

	@Override
	public String getExportData(Map<String, Object> payload) {

		String sExportData = "";
		try {
			Map<String, Object> vaSummaryMap = utils.getVASummaryList(payload, exportContext);
			sExportData = vaSummaryMap.get("vam_summary").toString();

		} catch (CBXBusinessException | CBXException e) {
			logger.debug("Exception {}", e.getStackTrace().toString());
		}
		return sExportData;
	}

}