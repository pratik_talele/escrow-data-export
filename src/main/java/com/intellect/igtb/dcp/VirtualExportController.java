package com.intellect.igtb.dcp;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.ICBXBackendResponse;
import com.intellect.igtb.dcp.export.controller.ExportServiceController;
import com.intellect.igtb.dcp.service.PrincipalExport;
import com.intellect.igtb.dcp.service.VaSummaryExportService;

/**
 * Rest Controller.
 */
@RestController
@RequestMapping("/export")
public class VirtualExportController {

	@Autowired
	VaSummaryExportService vaSummaryExportService;

	@Autowired
	private PrincipalExport principalExport;  
	
	/**
	  * {
	  *"summary_name":"vas",
	  *"format":"pdf",
	  *"refCurrEnabled":false,
	  *"isGrouped":true,
	  *"groupCriteria":"",
	  *"searchText":"TEST",
	  *"account_guid":""
	  *}
	  * action end point for generating virtual account summary
	  *
	  * @param payload the payload
	  * @param headers the headers
	  * @return ResponseEntity
	  */
	@RequestMapping(value = "/va-summary-list", method = RequestMethod.POST)
	public ResponseEntity<ICBXBackendResponse> vaSummaryList(@RequestBody final Map<String, Object> payload,
			@RequestHeader final Map<String, String> headers) {
		return new ExportServiceController(vaSummaryExportService).execute(payload, headers);
	}
	
	@RequestMapping(value = "/principal", method = RequestMethod.POST)
	public ResponseEntity<ICBXBackendResponse> principal(@RequestBody final Map<String, Object> payload,
			@RequestHeader final Map<String, String> headers) {
		return new ExportServiceController(principalExport).execute(payload, headers);
	}
	
	

}