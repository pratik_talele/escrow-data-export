package com.intellect.igtb.dcp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
/**
 * main class
 */
@ComponentScan("com.intellect.igtb.dcp")
public class VirtualExportApp {

	/**
	 * main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(VirtualExportApp.class, args);
	}

}
